<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data_insert = [];
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 100; $i++) {
            $data_insert[] = [
                'name'              => $faker->name,
                'email'             => $faker->unique()->email,
                'email_verified_at' => $faker->date(),
                'password'           => $faker->password,
            ];
        }
        DB::table('users')->insert($data_insert);
    }
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function () {
    $(document).on('click', '.e_validate_error', function (e) {
        validate_input($(this));
    });
    disablemousewhell();
    $(window).keydown(function (event) {
        if (event.keyCode === 13 && event.target.tagName !== "TEXTAREA") {
            console.log(event.target.tagName + " DISABLE ENTER KEY");
            event.preventDefault();
            return false;
        }
    });
    $('.select2').each(function (k, v) {
        $(v).attr('lang', 'ja');
    });
    // $.fn.select2.defaults.set('language', 'ja');
    disable_back_browser();
    ajaxloader();
    // $(document).toggleFullScreen();
    $('#fullscreen').click(function () {
        $(document).fullScreen(true);
    });
    $('#hidefullscreen').click(function () {
        $(document).fullScreen(false);
    });
    $(document).bind("fullscreenchange", function () {
        if ($(document).fullScreen()) {
            $('#fullscreen').hide();
            $('#hidefullscreen').show();
        } else {
            $('#fullscreen').show();
            $('#hidefullscreen').hide();
        }
        console.log("Fullscreen " + ($(document).fullScreen() ? "on" : "off"));
    });

    $(document).bind("fullscreenerror", function () {
        alert("Browser rejected fullscreen change");
    });

    window.onload = wrapTable;
    $("table.wrapTable").bind("DOMSubtreeModified", function () {
        wrapTable();
    });

    $(".btn-back").click(function () {
        window.history.back();
    });
    //iCheck for checkbox and radio inputs
    init_checkbox_radio();
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass   : 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass   : 'iradio_flat-green'
    });
    $('.e_logout_link').on('click', function () {
        doConfirm(function yes() {
            $('#logout-form').submit();
        }, function no() {
            // do nothing
        }, "ログアウトします。よろしいですか？", 'default');
    });
    //Remove entire tag with backspace
    $('.select2').on('select2:unselect', function (event) {
        $obj = $(this);
        setTimeout(function () {
            $('.select2-search__field', $obj.closest('.form-group')).val('');
        }, 0);
    });
    //Re-ordering tags
    $('.select2').on("select2:select", function (e) {
        var element = e.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
    });
    $("input").attr('autocomplete','off');
});

function validate_input(obj_event) {
    var obj = $(obj_event).closest('.form_validate_custom');
    obj.find('input').removeClass('has-error');
    obj.find('textarea').removeClass('has-error');
    obj.find('input').not('.bootstrap-filestyle input').not('.e_hide_template input').not('.e_not_allowed').each(function () {
        if (!$(this).val() || $(this).val().trim() == '') {
            $(this).addClass('has-error');
        }
    });
    obj.find('textarea').not('.e_hide_template textarea').each(function () {
        if (!$(this).val() || $(this).val().trim() == '') {
            $(this).addClass('has-error');
        }
    });
    obj.find('select').removeClass('has-error');
    obj.find('.select2-selection--multiple').removeClass('has-error');
    obj.find('select').not('.e_hide_template select').each(function () {
        if (!$(this).val() || $(this).val().trim() == '') {
            if($(this).hasClass('select2')){
                $(this).parent().find('.select2-selection--multiple').addClass('has-error');
            }else{
                $(this).addClass('has-error');
            }
        }
    });
}

function clear_validate_input(obj_event) {
    var obj = $(obj_event).closest('.form_validate_custom');
    obj.find('input').removeClass('has-error');
    obj.find('textarea').removeClass('has-error');
    obj.find('select').removeClass('has-error');
    obj.find('.select2-selection--multiple').removeClass('has-error');
}

function init_checkbox_radio() {
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass   : 'iradio_minimal-blue'
    });
}

function notify(msg, group = "alert-danger", urlRedirect = null) {
    // check type alert
    if (group.indexOf("alert-") == -1) {
        group = "alert-" + group;
    }
    // set icon by type alert
    var iconDefault = "fa fa-exclamation-triangle";
    if (group.indexOf("success") != -1) {
        iconDefault = "fa fa-check-circle";
    } else if (group.indexOf("error") != -1) {
        iconDefault = "fa fa-exclamation-triangle";
    }
    var html = '<div class="' + group + '">'
        + '<span>' + msg + '</span>'
        + '</div>';
    $.jGrowl(html, {
        group        : group,
        position     : 'top-right',
        sticky       : false,
        closeTemplate: false,
        header       : '<h4><i class="icon ' + iconDefault + '"></i>' + $('meta[name="msg-notify-header"]').attr('content') + '</h4>',
        animateOpen  : {
            width : 'show',
            height: 'show'
        },
        life         : 3000,
        close        : function (e, m, o) {
            if (urlRedirect) {
                window.location = urlRedirect;
            }
        }
    });
}

function disable_back_browser() {
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
}

function wrapTable() {
    $('.wrapTable td,.wrapTable th').not('.actionBtns').css('word-wrap', 'break-word');
    $('.wrapTable td,.wrapTable th').not('.actionBtns').css('min-width', $(window).width() * 0.1);
    $('.wrapTable td,.wrapTable th').not('.actionBtns').css('max-width', $(window).width() * 0.1);
}

function doConfirm(yesFn, noFn, msg, option, is_warning = false, is_upload = false) {
    if (!msg) {
        msg = $('meta[name="msg-confirm"]').attr('content');
    }
    switch (option) {
        case "danger":
            $('#confirmDialog .modal-body’').addClass('text-danger');
            break;
        case "success":
            $('#confirmDialog .modal-body’').addClass('text-success');
            break;
        case "warning":
            $('#confirmDialog .modal-body’').addClass('text-warning');
            break;
        case "default":
            $('#confirmDialog .modal-body’').addClass('');
            break;
        default:
            $('#confirmDialog .modal-body’').addClass('text-danger');
    }
    var confirmDialog = $("#confirmDialog");
    var confirmBox = $(confirmDialog).find("#confirmBox");
    confirmBox.find(".message").html(msg);
    confirmBox.find(".yes,.no,.confirmClose").unbind().click(function () {
        confirmDialog.hide();
    });
    confirmBox.find(".yes").click(yesFn);
    if (is_upload) {
        confirmBox.find(".yes").text('置き換え');
        confirmBox.find(".no").text('新規追加');
        confirmBox.find(".modal-footer").attr('text-align', '');
        confirmBox.find(".no").addClass('pull-right');
        confirmBox.find(".yes").show();
    } else {
        confirmBox.find(".yes").text('はい');
        if (is_warning) {
            confirmBox.find(".modal-footer").css('text-align', 'center');
            confirmBox.find(".no").removeClass('pull-right');
            confirmBox.find(".yes").hide();
            confirmBox.find(".no").text('閉じる');
        } else {
            confirmBox.find(".modal-footer").attr('text-align', '');
            confirmBox.find(".no").addClass('pull-right');
            confirmBox.find(".yes").show();
            confirmBox.find(".no").text('いいえ');
        }
    }
    confirmBox.find(".no").click(noFn);
    confirmBox.find(".confirmClose").click(noFn);
    confirmDialog.show();
}


// Valid JSON/JavaScript/Java escape sequences
var jsonEscapes = {
    oct    : true,
    unicode: true,
    "\\\\" : "\\",
    "\\\"" : "\"",
    "\\b"  : "\u0008",
    "\\f"  : "\u000C",
    "\\n"  : "\n",
    "\\r"  : "\r",
    "\\t"  : "\t"
};

// Character decoder for JSON/JavaScript/Java
var jsonCharDecoder = buildCharDecoder(jsonEscapes);

// Decodes a JSON/JavaScript/Java string
function fromJson(str) {
    str = str.replace(/^'(.*)'$|^"(.*)"$/, "$1$2");
    return parseString(str, jsonCharDecoder);
}

// Parse a string with a character handler function
function parseString(str, handler, joinChr) {
    var parser = {
        str   : str,
        peek  : function (max, re) {
            max = max || 1;
            var res = "";
            var pos = 0;
            while (res.length < max && this.str.length > pos) {
                var chr = this.str.charAt(pos++);
                if (re && !re.test(chr)) {
                    break;
                }
                res += chr;
            }
            return res;
        },
        read  : function (max, re) {
            var res = this.peek(max, re);
            this.str = this.str.substring(res.length);
            return res;
        },
        unread: function (chars) {
            this.str = chars + this.str;
        }
    };
    var res = [];
    while (parser.str.length > 0) {
        res.push(handler(parser.read(), parser));
    }
    return res.join(joinChr || "");
}

function buildCharDecoder(escapes) {
    return function (chr, parser) {
        var esc = chr + parser.peek();
        if (typeof (escapes[esc]) == "string") {
            parser.read();
            return escapes[esc];
        } else if (chr == "\\") {
            chr = parser.read();
            if (/[0-7]/.test(chr) && escapes.oct) {
                parser.unread(chr);
                var chrs = parser.read(3, /[0-7]/);
                var code = parseInt(chrs, 8);
                return String.fromCharCode(code);
            } else if (chr == "x" && escapes.hex) {
                var chrs = parser.read(2, /[0-9a-fA-F]/);
                var code = parseInt(chrs, 16);
                return String.fromCharCode(code);
            } else if (chr == "u" && escapes.unicode) {
                var chrs = parser.read(4, /[0-9a-fA-F]/);
                var code = parseInt(chrs, 16);
                return String.fromCharCode(code);
            } else {
                return chr;
            }
        } else {
            return chr;
        }
    };
}

function show_ajax_loading() {
    $('.e_loading').show();
}

function hide_ajax_loading() {
    $('.e_loading').hide();
}

//Ajax Loader
function ajaxloader() {
    var ajaxloading = '<div class="wait modal-backdrop fade in text-center" style="padding-top:25%;z-index:1549!important">' +
        '<i class = "fa fa-3x fa-refresh fa-spin" style="z-index:1550!important;color:white!important">' +
        '</i>' +
        '</div>';
    $(document).ajaxStart(function () {
        $('body').append(ajaxloading);
    });
    $(document).ajaxComplete(function () {
        $('.wait').remove();
    });
}

function setFixedThead(h) {

    $('.theadFixed tbody').css({
        'display'   : 'block',
        'max-height': h + 'px',
        'overflow-y': 'overlay'
    });

    $('.theadFixed tbody tr').css({
        'display'     : 'table',
        'width'       : '100%',
        'table-layout': 'fixed'
    });

    $('.theadFixed thead tr').css({
        'display'     : 'table',
        'width'       : '100%',
        'table-layout': 'fixed'

    });
}

function checkCell(str) {
    str = str.toUpperCase();
    //A1 notation general format
    const A1 = /^([A-Z]+)(\d+)$/;
    var row = 0;
    var col = 0;
    if (!A1.test(str)) {
        return false;
        notify("入力文字が不正です。");
    } else {
        var refParts = str.replace(A1, '$1,$2').split(',');
        var columnStr = refParts[0];
        row = refParts[1];
        for (var i = 0; i < columnStr.length; i++) {
            col = 26 * col + columnStr.charCodeAt(i) - 64;
        }
        if (row < 1048576 && col < 16384 && row != 0 && col != 0) {
            return true;
        } else {
            return false;
        }
    }

}


function checkChar(str) {
    str = str.charCodeAt();
    var ref = 0;
    switch (true) {
        case (str == 8221) || (str >= 12288 && str <= 12351) || (str >= 12352 && str <= 12447) || (str >= 12448 && str <= 12543) || (str >= 12784 && str <= 12799) || (str >= 12800 && str <= 13054) || (str >= 19968 && str <= 40879) || (str >= 13312 && str <= 19903):
            //Normal character and JP special char
            ref = 3;
            break;
        case (str >= 65280 && str <= 65376) || (str >= 65504 && str <= 65510):
            //full width roman character (Zen Kaku)
            ref = 1;
            break;
        case (str >= 65377 && str <= 65439) || (str >= 65512 && str <= 65518):
            //half width character (Han Kaku)
            ref = 2;
            break;
        default:
            //Other Char and non-JP Char (Include Latin char, other nation char)
            ref = 0;
            break;
    }
    return ref;
}

function setDisabled(s) {
    $(s).each(function () {
        var wrap = "<div></div>";
        wrap = $(wrap).css({
            'cursor' : 'not-allowed',
            'opacity': '.5',
            'filter' : 'alpha(opacity=50)',
            'z-index': '9999',
            'padding': '0 0 0 0'
        });
        $(this).css('pointer-events', 'none').wrap(wrap);
    });
    return "DEBUG set disabled stats";
}

function unsetDisabled(s) {
    $(s).each(function () {
        $(this).css('pointer-events', '').unwrap();
    });
    return "DEBUG unset disabled stats";
}

function compareD(fromD, toD) {
    if (fromD === "" && toD === "") {
        return true;
    } else {
        var d1 = new Date(fromD.split('/')[0], fromD.split('/')[1], fromD.split('/')[2]);
        var d2 = new Date(toD.split('/')[0], toD.split('/')[1], toD.split('/')[2]);
        if (d1 < d2){
            return true;
        }else{
            return false;
        }
    }
}

function disablemousewhell() {
    $('.pull-right-container').closest('a').removeAttr('href');
    $('.fa-truck').closest('a').removeAttr('href');
}

String.prototype.replaceAll = function (str1, str2, ignore) {
    return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, "\\$&"), (ignore ? "gi" : "g")), (typeof (str2) == "string") ? str2.replace(/\$/g, "$$$$") : str2);
};

function cvtJPNumb(str) {
    var jn = ['０', '１', '２', '３', '４', '５', '６', '７', '８', '９','Ａ','Ｂ','Ｃ','Ｄ','Ｅ','Ｆ','Ｇ','Ｈ','Ｉ','Ｊ','Ｋ','Ｌ','Ｍ','Ｎ','Ｏ','Ｐ','Ｑ','Ｒ','Ｓ','Ｔ','Ｕ','Ｖ','W','Ｘ','Ｙ','Ｚ'];
    var en = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    for (var i = 0; i < jn.length; i++) {
        str = str.replaceAll(jn[i], en[i]);
    }
    return str;
}

/*S: TrungHQ4 - ADD - function makeid for create an random string for make an token for OB,Esca,Defect Managerment - 2018-09-16*/

function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 12; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

/*E: TrungHQ4 - ADD - function makeid for create an random string for make an token for OB,Esca,Defect Managerment - 2018-09-16*/

function showProgressModal(title, message) {
    $(window).bind('beforeunload', function (e) {
        return "You have some unsaved changes";
    });
    var progressModal = $('#progressModal');
    progressModal.find(".modal-title b").text(title);
    progressModal.find(".modal-message").text(message);
    progressModal.modal({
        backdrop: 'static',
        keyboard: false
    });
}

function hideProgressModal(title, message) {
    $(window).off("beforeunload");
    var progressModal = $('#progressModal');
    progressModal.modal('hide');
    progressModal.find('.progress-bar-primary').attr('style', 'width: 0%');
    progressModal.find('span').css('color', '');
    progressModal.find('span').html(0 + '%');
}

function updateProgressModal(percent) {
    var progressModal = $('#progressModal');
    progressModal.find('.progress-bar-primary').attr('style', 'width: ' + percent + '%');
    progressModal.find('span').html(percent + '%');
    if (percent > 50) {
        progressModal.find('span').css('color', '#FFF');
    }
}
<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BaseModel extends Model {
    public function get_list_filter($whereCondition = null, $whereInCondition = null, $likeCondition = null, $specialCondition = null,$field = null, $limit = 0, $post = 0, $order = null) {
        $query = DB::table($this->table);
        $query = $this->set_select_fields($query, $field);
        if (is_array($whereCondition) && !empty($whereCondition)) {
            foreach ($whereCondition as $field => $condition) {
                $query->where($field, $condition);
            }
        }
        if (is_array($whereInCondition) && !empty($whereInCondition)) {
            foreach ($whereInCondition as $field => $condition) {
                if (is_array($condition) && !empty($condition)) {
                    $query->whereIn($field, $condition);
                }
            }
        }
        if (is_array($likeCondition) && !empty(!empty($likeCondition))) {

        }
        $query = $this->set_special_conditions($query,$specialCondition);
        return $query->get();
    }

    public function set_select_fields($query, $field) {
        if (is_array($field) && !empty($field)) {
            foreach ($field as $item) {
                $query->select($item);
            }
        }
        return $query;
    }

    public function set_special_conditions($query,$specialCondition){

        return $query;
    }
}

<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Users.
 *
 * @package namespace App\Entities;
 */
class Users extends BaseModel implements Transformable
{
    use TransformableTrait;
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    public function data_field(){
        return [
            'user_name' => [
                'label' => 'Tên người dùng',
                'type' => 'input',
                'value' => '',
                'required' => true,
                'placeholder' => 'Vui lòng nhập tên người dùng',
            ],
            'email' => [],
        ];
    }
}

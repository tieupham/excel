<?php

namespace App\Http\Controllers;

use App\Entities\Users;
use App\Http\Controllers\Base\LayoutsController;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\UsersCreateRequest;
use App\Http\Requests\UsersUpdateRequest;
use App\Repositories\UsersRepository;
use App\Validators\UsersValidator;

/**
 * Class UsersController.
 *
 * @package namespace App\Http\Controllers;
 */
class UsersController extends LayoutsController {
    /**
     * @var UsersRepository
     */
    protected $repository;

    /**
     * @var UsersValidator
     */
    protected $validator;

    /**
     * UsersController constructor.
     *
     * @param UsersRepository $repository
     * @param UsersValidator $validator
     */
    public function __construct(UsersRepository $repository, UsersValidator $validator) {
        parent::__construct();
        $this->repository = $repository;
        $this->validator = $validator;
    }


    public function change_course() {
        ini_set('max_execution_time', 360);
        ini_set('memory_limit', '-1');
        $reader = new Xlsx();
        $spreadsheet = $reader->load(public_path('data.xlsx'));
        $spreadsheet = $spreadsheet->getActiveSheet()->toArray();
        $new_file = new Spreadsheet();
        $file_write = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($new_file);
        $new_file->setActiveSheetIndex(0);
        $new_file = $new_file->getActiveSheet();
        $new_file->setCellValue('A1', 'First Name');
        $new_file->setCellValue('B1', 'Last Name');
        $new_file->setCellValue('C1', 'Employee ID');
        $new_file->setCellValue('D1', 'CourseNumber');
        $new_file->setCellValue('E1', 'CourseName');
        $new_file->setCellValue('F1', 'EnrollDate');
        $new_file->setCellValue('G1', 'CompletionDate');
        $new_file->setCellValue('H1', 'Passed');
        $new_file->setCellValue('I1', 'TrainingHrs');
        $new_file->setCellValue('J1', 'Score');
        $count = count($spreadsheet);
        $line = 2;
        $course_number = [
            ['name' => 'Scorm890', 'time' => 85],
            ['name' => 'Scorm891', 'time' => 36],
        ];
        $course_name = [
            'Scorm890' => 'Phần B - Quy định pháp luật về hoạt động kinh doanh theo phương thức đa cấp',
            'Scorm891' => 'Phần C - Quy định pháp luật về hoạt động kinh doanh theo phương thức đa cấp',
        ];
        for ($m = 1; $m < $count; $m++) {
            if (empty($spreadsheet[$m][0])) {
                break;
            }
            //            dd($spreadsheet[$m][4]);
            //            $time = $this->make_time_new_source($spreadsheet[$m][2]);
            //            for ($n = 0; $n < 2; $n++) {
            //                $user_score = $score[rand(0, 4)];
            $start = date('m/d/Y H:i:s',strtotime(str_replace('/','-',$spreadsheet[$m][4])));
            $end = date('m/d/Y H:i:s',strtotime(str_replace('/','-',$spreadsheet[$m][5])));
            $new_file->setCellValue('A' . $line, $spreadsheet[$m][0]);
            $new_file->setCellValue('B' . $line, null);
            $new_file->setCellValue('C' . $line, $spreadsheet[$m][1]);
            $new_file->setCellValue('D' . $line, 'Scorm904');
            $new_file->setCellValue('E' . $line, 'Bài kiểm tra kiến thức pháp luật bán hàng đa cấp');
            $new_file->setCellValue('F' . $line, $start);
            $new_file->setCellValue('G' . $line, $end);
            $new_file->setCellValue('H' . $line, 'Yes');
            $new_file->setCellValue('I' . $line, date('H:i', mktime(0, $spreadsheet[$m][7])));
            $new_file->setCellValue('J' . $line, !empty($spreadsheet[$m][8] && $spreadsheet[$m][8] != 'NULL' ) ? $spreadsheet[$m][8] : 90);
            $line++;
            //            }

            //            $new_file->setCellValue('A' . ($i + 1), $spreadsheet[$i][0]);
            //            $new_file->setCellValue('B' . ($i + 1), $spreadsheet[$i][1]);
            //            $new_file->setCellValue('C' . ($i + 1), $end);
        }
        $file = 'format_file_result';
        $file_write->save($file . '.xlsx');

        return response()->download(public_path($file . '.xlsx'));
    }


    public function import_format() {
        return view('import');
    }

    public function import_format_cell() {
        return view('import_format_cell');
    }

    public function format_cell() {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '512M');
        $reader = new Xlsx();
        $spreadsheet = $reader->load(public_path('data.xlsx'));
        $spreadsheet = $spreadsheet->getActiveSheet()->toArray();
        //        unset($spreadsheet[0]);
        $new_file = new Spreadsheet();
        $file_write = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($new_file);
        $new_file->setActiveSheetIndex(0);
        $new_file = $new_file->getActiveSheet();


        //        $new_file->setCellValue('A1', 'UserName');
        //        $new_file->setCellValue('B1', 'Full Name');
        //                $new_file->setCellValue('C1', 'Registered Date');
        //                $new_file->setCellValue('D1', 'Activation Date');
        //        $new_file->setCellValue('C1', 'ENROLL_DATE');
        $count = count($spreadsheet);
        for ($i = 1; $i < $count; $i++) {
            //            $reg = $spreadsheet[$i][2];
            //            $active = $spreadsheet[$i][3];
            //            if (empty($active)) {
            //                $end = $reg;
            //            } else {
            //                //                $start = $active;
            //                $end = $active;
            //            }

            $active = !empty($spreadsheet[$i][3] && $spreadsheet[$i][3] != '#N/A') ? date('m/d/Y H:i:s', strtotime($spreadsheet[$i][3])) : null;
            $new_file->setCellValue('A' . ($i), $spreadsheet[$i][0]);
            $new_file->setCellValue('B' . ($i), $spreadsheet[$i][1]);
            $new_file->setCellValue('C' . ($i), date('m/d/Y H:i:s', strtotime($spreadsheet[$i][2])));
            $new_file->setCellValue('D' . ($i), $active);
            //            $new_file->setCellValue('C' . $i, date('m/d/Y H:i:s', strtotime($end)));
            //            $new_file->setCellValue('D' . ($i+1), $end);
        }
        $file = 'format_cell';
        $file_write->save($file . '.xlsx');

        return response()->download(public_path($file . '.xlsx'));
    }

    public function format_time() {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '-1');
        $reader = new Xlsx();
        $spreadsheet = $reader->load(public_path('format_cell.xlsx'));
        $spreadsheet = $spreadsheet->getActiveSheet()->toArray();
        $new_file = new Spreadsheet();
        $file_write = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($new_file);
        $new_file->setActiveSheetIndex(0);
        $new_file = $new_file->getActiveSheet();

        $count = count($spreadsheet);
        for ($i = 0; $i < $count; $i++) {
            $reg = $spreadsheet[$i][2];
            $active = $spreadsheet[$i][3];
            if (empty($active)) {
                $end = date('m/d/Y H:i:s', strtotime($reg . ' +24 hours'));
            } else {
                $end = $active;
            }

            $new_file->setCellValue('A' . ($i + 1), $spreadsheet[$i][0]);
            $new_file->setCellValue('B' . ($i + 1), $spreadsheet[$i][1]);
            $new_file->setCellValue('C' . ($i + 1), $end);
        }
        $file = 'format_time_result';
        $file_write->save($file . '.xlsx');

        return response()->download(public_path($file . '.xlsx'));
    }

    public function make_file() {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '-1');
        $reader = new Xlsx();
        $spreadsheet = $reader->load(public_path('format_time_result_16.xlsx'));
        $spreadsheet = $spreadsheet->getActiveSheet()->toArray();
        $new_file = new Spreadsheet();
        $file_write = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($new_file);
        $new_file->setActiveSheetIndex(0);
        $new_file = $new_file->getActiveSheet();
        $new_file->setCellValue('A1', 'First Name');
        $new_file->setCellValue('B1', 'Last Name');
        $new_file->setCellValue('C1', 'Employee ID');
        $new_file->setCellValue('D1', 'CourseNumber');
        $new_file->setCellValue('E1', 'CourseName');
        $new_file->setCellValue('F1', 'EnrollDate');
        $new_file->setCellValue('G1', 'CompletionDate');
        $new_file->setCellValue('H1', 'Passed');
        $new_file->setCellValue('I1', 'TrainingHrs');
        $new_file->setCellValue('J1', 'Score');
        $count = count($spreadsheet);
        $line = 2;
        $score = [80, 85, 90, 95, 100];
        $course_number = $this->course();
        $course_name = $this->course_name();
        for ($m = 0; $m < $count; $m++) {
            if (empty($spreadsheet[$m][0])) {
                break;
            }
            $time = $this->make_time($spreadsheet[$m][3]);
            for ($n = 0; $n < 8; $n++) {
                $user_score = $n == 7 ? $score[rand(0, 4)] : null;
                $new_file->setCellValue('A' . $line, $spreadsheet[$m][2]);
                $new_file->setCellValue('B' . $line, null);
                $new_file->setCellValue('C' . $line, $spreadsheet[$m][1]);
                $new_file->setCellValue('D' . $line, $course_number[$n]['name']);
                $new_file->setCellValue('E' . $line, $course_name[$course_number[$n]['name']]);
                $new_file->setCellValue('F' . $line, $time['time_start'][$n]);
                $new_file->setCellValue('G' . $line, $time['time_end'][$n]);
                $new_file->setCellValue('H' . $line, 'Yes');
                $new_file->setCellValue('I' . $line, $time['diff_time'][$n]);
                $new_file->setCellValue('J' . $line, $user_score);
                $line++;
            }

            //            $new_file->setCellValue('A' . ($i + 1), $spreadsheet[$i][0]);
            //            $new_file->setCellValue('B' . ($i + 1), $spreadsheet[$i][1]);
            //            $new_file->setCellValue('C' . ($i + 1), $end);
        }
        $file = 'format_file_result';
        $file_write->save($file . '.xlsx');

        return response()->download(public_path($file . '.xlsx'));
    }

    public function make_file_add_two_course() {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '-1');
        $reader = new Xlsx();
        $spreadsheet = $reader->load(public_path('format_time_result_6.xlsx'));
        $spreadsheet = $spreadsheet->getActiveSheet()->toArray();
        $new_file = new Spreadsheet();
        $file_write = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($new_file);
        $new_file->setActiveSheetIndex(0);
        $new_file = $new_file->getActiveSheet();
        $new_file->setCellValue('A1', 'First Name');
        $new_file->setCellValue('B1', 'Last Name');
        $new_file->setCellValue('C1', 'Employee ID');
        $new_file->setCellValue('D1', 'CourseNumber');
        $new_file->setCellValue('E1', 'CourseName');
        $new_file->setCellValue('F1', 'EnrollDate');
        $new_file->setCellValue('G1', 'CompletionDate');
        $new_file->setCellValue('H1', 'Passed');
        $new_file->setCellValue('I1', 'TrainingHrs');
        $new_file->setCellValue('J1', 'Score');
        $count = count($spreadsheet);
        $line = 2;
        $score = [80, 85, 90, 95, 100];
        $course_number = [
            ['name' => 'Scorm890', 'time' => 85],
            ['name' => 'Scorm891', 'time' => 36],
        ];
        $course_name = [
            'Scorm890' => 'Phần B - Quy định pháp luật về hoạt động kinh doanh theo phương thức đa cấp',
            'Scorm891' => 'Phần C - Quy định pháp luật về hoạt động kinh doanh theo phương thức đa cấp',
        ];
        for ($m = 0; $m < $count; $m++) {
            if (empty($spreadsheet[$m][0])) {
                break;
            }
            $time = $this->make_time_new_source($spreadsheet[$m][2]);
            for ($n = 0; $n < 2; $n++) {
                //                $user_score = $score[rand(0, 4)];
                $new_file->setCellValue('A' . $line, $spreadsheet[$m][3]);
                $new_file->setCellValue('B' . $line, null);
                $new_file->setCellValue('C' . $line, $spreadsheet[$m][1]);
                $new_file->setCellValue('D' . $line, $course_number[$n]['name']);
                $new_file->setCellValue('E' . $line, $course_name[$course_number[$n]['name']]);
                $new_file->setCellValue('F' . $line, $time['time_start'][$n]);
                $new_file->setCellValue('G' . $line, $time['time_end'][$n]);
                $new_file->setCellValue('H' . $line, 'Yes');
                $new_file->setCellValue('I' . $line, $time['diff_time'][$n]);
                //                $new_file->setCellValue('J' . $line, $user_score);
                $line++;
            }

            //            $new_file->setCellValue('A' . ($i + 1), $spreadsheet[$i][0]);
            //            $new_file->setCellValue('B' . ($i + 1), $spreadsheet[$i][1]);
            //            $new_file->setCellValue('C' . ($i + 1), $end);
        }
        $file = 'format_file_result';
        $file_write->save($file . '.xlsx');

        return response()->download(public_path($file . '.xlsx'));
    }

    public function make_file_add_two_course_advance() {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '-1');
        $reader = new Xlsx();
        $spreadsheet = $reader->load(public_path('format_time_result_1.xlsx'));
        $spreadsheet = $spreadsheet->getActiveSheet()->toArray();
        $new_file = new Spreadsheet();
        $file_write = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($new_file);
        $new_file->setActiveSheetIndex(0);
        $new_file = $new_file->getActiveSheet();
        $new_file->setCellValue('A1', 'First Name');
        $new_file->setCellValue('B1', 'Last Name');
        $new_file->setCellValue('C1', 'Employee ID');
        $new_file->setCellValue('D1', 'CourseNumber');
        $new_file->setCellValue('E1', 'CourseName');
        $new_file->setCellValue('F1', 'EnrollDate');
        $new_file->setCellValue('G1', 'CompletionDate');
        $new_file->setCellValue('H1', 'Passed');
        $new_file->setCellValue('I1', 'TrainingHrs');
        $new_file->setCellValue('J1', 'Score');
        $count = count($spreadsheet);
        $line = 2;
        $score = [80, 85, 90, 95, 100];
        $course_number = [
            ['name' => 'Scorm890', 'time' => 85],
            ['name' => 'Scorm891', 'time' => 36],
        ];
        $course_name = [
            'Scorm890' => 'Phần B - Quy định pháp luật về hoạt động kinh doanh theo phương thức đa cấp',
            'Scorm891' => 'Phần C - Quy định pháp luật về hoạt động kinh doanh theo phương thức đa cấp',
        ];
        for ($m = 0; $m < $count; $m++) {
            if (empty($spreadsheet[$m][0])) {
                break;
            }
            $time = $this->make_time_new_source_advance($spreadsheet[$m][2]);
            for ($n = 0; $n < 2; $n++) {
                $user_score = $score[rand(0, 4)];
                $new_file->setCellValue('A' . $line, $spreadsheet[$m][3]);
                $new_file->setCellValue('B' . $line, null);
                $new_file->setCellValue('C' . $line, $spreadsheet[$m][1]);
                $new_file->setCellValue('D' . $line, $course_number[$n]['name']);
                $new_file->setCellValue('E' . $line, $course_name[$course_number[$n]['name']]);
                $new_file->setCellValue('F' . $line, $time['time_start'][$n]);
                $new_file->setCellValue('G' . $line, $time['time_end'][$n]);
                $new_file->setCellValue('H' . $line, 'Yes');
                $new_file->setCellValue('I' . $line, $time['diff_time'][$n]);
                $new_file->setCellValue('J' . $line, $user_score);
                $line++;
            }

            //            $new_file->setCellValue('A' . ($i + 1), $spreadsheet[$i][0]);
            //            $new_file->setCellValue('B' . ($i + 1), $spreadsheet[$i][1]);
            //            $new_file->setCellValue('C' . ($i + 1), $end);
        }
        $file = 'format_file_result';
        $file_write->save($file . '.xlsx');

        return response()->download(public_path($file . '.xlsx'));
    }

    public function make_time_new_source_advance($time) {
        $time_arr = explode(' ', $time);
        $h = rand(14, 23);
        $m = rand(4, 59);
        $s = rand(0, 59);
        //        $time = $time_arr[0] . ' ' . str_pad($h, 2, "0", STR_PAD_LEFT) . ':' . str_pad($m, 2, "0", STR_PAD_LEFT) . ':' . str_pad($s, 2, "0", STR_PAD_LEFT);
        //trừ dần
        $arr_time = [85, 36];
        $data_end = $data_start = $diff_seconds = [];
        for ($i = 0; $i <= 1; $i++) {
            $range_mininus = rand(12, 28);
            $ran = rand(3, 30);
            $range_second = rand(5, 58);
            if ($i == 1) {
                $data_start[$i] = date('m/d/Y H:i:s', strtotime($data_end[$i - 1] . ' +' . $ran . ' minutes +' . ($range_second + 7) . ' seconds'));
                //                $data_start[$i] = date('m/d/Y H:i:s', strtotime($data_end[$i] . ' -' . ($arr_time[$i] + $range_mininus) . ' minutes -' . ($range_second) . ' seconds'));
            } else {
                $data_start[$i] = date('m/d/Y H:i:s', strtotime($time . ' +' . $ran . ' minutes +' . ($range_second + 7) . ' seconds'));
                //                $data_start[$i] = date('m/d/Y H:i:s', strtotime($time . ' -30 minutes'));
            }
            $data_end[$i] = date('m/d/Y H:i:s', strtotime($data_start[$i] . ' +' . ($arr_time[$i] + $range_mininus) . ' minutes +' . ($range_second) . ' seconds'));
            $diff_seconds[$i] = gmdate('H:i:s', strtotime($data_end[$i]) - strtotime($data_start[$i]));
        }
        return ['time_start' => $data_start, 'time_end' => $data_end, 'diff_time' => $diff_seconds];
    }

    public function make_time_new_source($time) {
        $time_arr = explode(' ', $time);
        $h = rand(14, 23);
        $m = rand(4, 59);
        $s = rand(0, 59);
        //        $time = $time_arr[0] . ' ' . str_pad($h, 2, "0", STR_PAD_LEFT) . ':' . str_pad($m, 2, "0", STR_PAD_LEFT) . ':' . str_pad($s, 2, "0", STR_PAD_LEFT);
        //trừ dần
        $arr_time = [85, 36];
        $data_end = $data_start = $diff_seconds = [];
        for ($i = 1; $i >= 0; $i--) {
            $range_mininus = rand(12, 28);
            $ran = rand(3, 30);
            $range_second = rand(5, 58);
            if ($i == 0) {
                $data_end[$i] = date('m/d/Y H:i:s', strtotime($data_start[$i + 1] . ' -' . $ran . ' minutes -' . ($range_second + 7) . ' seconds'));
                //                $data_start[$i] = date('m/d/Y H:i:s', strtotime($data_end[$i] . ' -' . ($arr_time[$i] + $range_mininus) . ' minutes -' . ($range_second) . ' seconds'));
            } else {
                $data_end[$i] = date('m/d/Y H:i:s', strtotime($time . ' -' . $ran . ' minutes -' . ($range_second + 7) . ' seconds'));
                //                $data_start[$i] = date('m/d/Y H:i:s', strtotime($time . ' -30 minutes'));
            }
            $data_start[$i] = date('m/d/Y H:i:s', strtotime($data_end[$i] . ' -' . ($arr_time[$i] + $range_mininus) . ' minutes -' . ($range_second) . ' seconds'));
            $diff_seconds[$i] = gmdate('H:i:s', strtotime($data_end[$i]) - strtotime($data_start[$i]));
        }
        return ['time_start' => $data_start, 'time_end' => $data_end, 'diff_time' => $diff_seconds];
    }

    public function make_time($time) {
        $time_arr = explode(' ', $time);
        $h = rand(14, 23);
        $m = rand(4, 59);
        $s = rand(0, 59);
        $time = $time_arr[0] . ' ' . str_pad($h, 2, "0", STR_PAD_LEFT) . ':' . str_pad($m, 2, "0", STR_PAD_LEFT) . ':' . str_pad($s, 2, "0", STR_PAD_LEFT);
        //trừ dần
        $arr_time = [8, 19, 197, 85, 40, 70, 60, 30];
        $data_end = $data_start = $diff_seconds = [];
        for ($i = 7; $i >= 0; $i--) {
            $range_mininus = rand(7, 18);
            $ran = rand(3, 30);
            $range_second = rand(5, 58);
            if ($i != 7) {
                $data_end[$i] = date('m/d/Y H:i:s', strtotime($data_start[$i + 1] . ' -' . $ran . ' minutes -' . ($range_second + 7) . ' seconds'));
                $data_start[$i] = date('m/d/Y H:i:s', strtotime($data_end[$i] . ' -' . ($arr_time[$i] + $range_mininus) . ' minutes -' . ($range_second) . ' seconds'));
                //                $sub_start = substr($data_start[$i], 11, 2);
                //                $sub_end = substr($data_start[$i], 11, 2);
                //                if ($sub_end < 12) {
                //                    if ($sub_start == 5) {
                //                        $data_start[$i] = date('m/d/Y H:i:s', strtotime($data_start[$i] . ' -60 minutes'));
                //                    }
                //                } else if ($sub_end < 5) {
                //                    if ($sub_start == 12) {
                //                        $data_start[$i] = date('m/d/Y H:i:s', strtotime($data_start[$i] . ' -60 minutes'));
                //                    }
                //                } else if ($sub_end == 12) {
                //                    $data_end[$i] = date('m/d/Y H:i:s', strtotime($data_end[$i] . ' -60 minutes'));
                //                    $data_start[$i] = date('m/d/Y H:i:s', strtotime($data_start[$i] . ' -60 minutes'));
                //                    $sub_start = substr($data_start[$i], 6, 2);
                //                    if ($sub_start == 5) {
                //                        $data_start[$i] = date('m/d/Y H:i:s', strtotime($data_start[$i] . ' -60 minutes'));
                //                    }
                //                } else if ($sub_end == 5) {
                //                    $data_end[$i] = date('m/d/Y H:i:s', strtotime($data_end[$i] . ' -60 minutes'));
                //                    $data_start[$i] = date('m/d/Y H:i:s', strtotime($data_start[$i] . ' -60 minutes'));
                //                    $sub_start = substr($data_start[$i], 6, 2);
                //                    if ($sub_start == 12) {
                //                        $data_start[$i] = date('m/d/Y H:i:s', strtotime($data_start[$i] . ' -60 minutes'));
                //                    }
                //                } else {
                //
                //                }
            } else {
                $data_end[$i] = $time;
                $data_start[$i] = date('m/d/Y H:i:s', strtotime($time . ' -30 minutes'));
            }
            $diff_seconds[$i] = gmdate('H:i:s', strtotime($data_end[$i]) - strtotime($data_start[$i]));
        }
        return ['time_start' => $data_start, 'time_end' => $data_end, 'diff_time' => $diff_seconds];
    }

    public function upload_file_format(Request $request) {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '512M');
        try {
            $reader = new Xlsx();
            //        $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($request->file);
            $spreadsheet = $spreadsheet->getActiveSheet()->toArray();
            $new_file = new Spreadsheet();
            $file_write = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($new_file);
            $new_file->setActiveSheetIndex(0);
            $new_file = $new_file->getActiveSheet();
            $new_file->setCellValue('A1', 'First Name');
            $new_file->setCellValue('B1', 'Last Name');
            $new_file->setCellValue('C1', 'Employee ID');
            $new_file->setCellValue('D1', 'CourseNumber');
            $new_file->setCellValue('E1', 'CourseName');
            $new_file->setCellValue('F1', 'EnrollDate');
            $new_file->setCellValue('G1', 'CompletionDate');
            $new_file->setCellValue('H1', 'Passed');
            $new_file->setCellValue('I1', 'TrainingHrs');
            $new_file->setCellValue('J1', 'Score');

            $course_number = [
                'Scorm881' => 8,
                'Scorm862' => 19,
                'Scorm870' => 197,
                'Scorm863' => 85,
                'Scorm875' => 40,
                'Scorm876' => 70,
                'Scorm873' => 60,
                'Scorm882' => 30,
            ];
            $data_return = $this->format_data_spreadsheet($spreadsheet);
            $index = 2;
            foreach ($data_return as $sub_data) {
                if (!empty($sub_data)) {
                    $start = explode(' ', $sub_data[0][5]);
                    $start = $start[0];
                    $count_line = count($sub_data);
                    $end = explode(' ', $sub_data[$count_line - 1][6]);
                    $end = $end[0];
                    $start_h = null;
                    $start_m = null;
                    $flag = true;
                    if ($start == $end) {
                        if (empty($start_h)) {
                            $r = rand(6, 12);
                            $start_h = rand(0, $r);
                        }
                        if (empty($start_m)) {
                            $start_m = str_pad(rand(0, 59), 2, "0", STR_PAD_LEFT);
                        }
                        $range = 12 - $start_h;
                        $range = floor((($range + 2) * 60 + 59) / 8);
                    } else {
                        if (empty($start_h)) {
                            $start_h = rand(14, 23);
                        }
                        if (empty($start_m)) {
                            $start_m = rand(0, 59);
                        }
                        $range = floor(30 * 60 / 8);
                    }

                    for ($temp_i = 0; $temp_i < $count_line; $temp_i++) {
                        if (!empty($sub_data[$temp_i][0])) {
                            $show_start = $start;
                            if (!$flag) {
                                $show_start = $end;
                            }
                            $start_show = $show_start . ' ' . str_pad($start_h, 2, "0", STR_PAD_LEFT) . ':' . str_pad($start_m, 2, "0", STR_PAD_LEFT) . ':00';
                            $temp_r = rand(0, $range / 2);
                            if ($temp_i != 7) {
                                $m = $start_m + $course_number[$sub_data[$temp_i][3]] + $temp_r;
                            } else {
                                $m = $start_m + 30;
                            }
                            $h = floor($m / 60);
                            $m = $m - $h * 60;
                            $start_h = $start_h + $h;
                            $temp_end = $end;
                            if ($start != $end && $start_h < 24) {
                                $end = $start;
                            }
                            if ($start_h > 23) {
                                $flag = false;
                                $start_h = $start_h - 24;
                                $start = $temp_end;
                            } else {
                                $flag = true;
                            }
                            if (strtotime($end) - strtotime($show_start) < 0) {
                                $end_show = $show_start . ' ' . str_pad($start_h, 2, "0", STR_PAD_LEFT) . ':' . str_pad($m, 2, "0", STR_PAD_LEFT) . ':00';
                            } else {
                                $end_show = $end . ' ' . str_pad($start_h, 2, "0", STR_PAD_LEFT) . ':' . str_pad($m, 2, "0", STR_PAD_LEFT) . ':00';
                            }
                            $start_m = $m + rand(5, $range / 2);
                            $end = $temp_end;

                            $start_m = $start_m == 60 ? 63 : $start_m;
                            if ($start_m > 60) {
                                $h = floor($start_m / 60);
                                $start_m = $start_m - $h * 60;
                                $start_h = $start_h + $h;
                                if ($start_h > 23) {
                                    $flag = false;
                                    $start_h = $start_h - 24;
                                } else {
                                    $flag = true;
                                }
                            }
                            $t_1 = strtotime($end_show);
                            $t_2 = strtotime($start_show);
                            $t_h = floor(($t_1 - $t_2) / 3600);
                            $new_file->setCellValue('A' . $index, $sub_data[$temp_i][0]);
                            $new_file->setCellValue('B' . $index, $sub_data[$temp_i][1]);
                            $new_file->setCellValue('C' . $index, $sub_data[$temp_i][2]);
                            $new_file->setCellValue('D' . $index, $sub_data[$temp_i][3]);
                            $new_file->setCellValue('E' . $index, $sub_data[$temp_i][4]);
                            $new_file->setCellValue('F' . $index, $start_show);
                            $new_file->setCellValue('G' . $index, $end_show);
                            $new_file->setCellValue('H' . $index, $sub_data[$temp_i][7]);
                            $new_file->setCellValue('I' . $index, str_pad($t_h, 2, "0", STR_PAD_LEFT) . ':' . str_pad((($t_1 - $t_2 - $t_h * 3600) / 60), 2, "0", STR_PAD_LEFT) . ':00');
                            $new_file->setCellValue('J' . $index, empty($sub_data[$temp_i][9]) ? null : $sub_data[$temp_i][9]);
                            $index++;
                        }
                    }
                }
            }
            $file_write->save('format.xlsx');
            return response()->download(public_path('format.xlsx'));
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function gk_excell() {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '512M');
        $reader = new Xlsx();
        //        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load(public_path('data_file.xlsx'));
        $spreadsheet = $spreadsheet->getActiveSheet()->toArray();
        $course_number = $this->course();
        $course_name = $this->course_name();
        //First_name => 2
        //Last_name => null
        //ID => 1
        //course_number => $couerse_number
        //course_name => $course_name
        //EnrollDate => 6
        //CompletionDate => 7
        //Passed => "Yes"
        //TrainingHrs => 7-6
        //Score => null
        $count = count($spreadsheet);
        $new_file = new Spreadsheet();
        $file_write = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($new_file);
        $new_file->setActiveSheetIndex(0);
        $new_file = $new_file->getActiveSheet();
        $new_file->setCellValue('A1', 'First Name');
        $new_file->setCellValue('B1', 'Last Name');
        $new_file->setCellValue('C1', 'Employee ID');
        $new_file->setCellValue('D1', 'CourseNumber');
        $new_file->setCellValue('E1', 'CourseName');
        $new_file->setCellValue('F1', 'EnrollDate');
        $new_file->setCellValue('G1', 'CompletionDate');
        $new_file->setCellValue('H1', 'Passed');
        $new_file->setCellValue('I1', 'TrainingHrs');
        $new_file->setCellValue('J1', 'Score');
        $line = 2;
        $score = [80, 85, 90, 95, 100];
        for ($i = 1; $i < $count; $i++) {
            $start = $spreadsheet[$i][6];
            $end = $spreadsheet[$i][7];
            $start_h = null;
            $start_m = null;
            $flag = true;
            $f = true;
            if ($start == $end) {
                if (empty($start_h)) {
                    $r = rand(0, 12);
                    $start_h = rand(0, $r);
                }
                if (empty($start_m)) {
                    $start_m = str_pad(rand(0, 59), 2, "0", STR_PAD_LEFT);
                }
                $range = 12 - $start_h;
                $range = floor((($range + 2) * 60 + 59) / 8);
            } else {
                if (empty($start_h)) {
                    $start_h = rand(19, 23);
                }
                if (empty($start_m)) {
                    $start_m = rand(0, 59);
                }
                $range = floor(15 * 60 / 8);
            }
            for ($j = 0; $j < 8; $j++) {
                $show_start = $start;
                if (!$flag) {
                    $show_start = $end;
                }
                $start_show = $show_start . ' ' . str_pad($start_h, 2, "0", STR_PAD_LEFT) . ':' . str_pad($start_m, 2, "0", STR_PAD_LEFT) . ':00';
                $temp_r = rand(0, $range / 2);
                if ($j != 7) {
                    $m = $start_m + $course_number[$j]['time'] + $temp_r;
                } else {
                    $m = $start_m + 30;
                }
                $h = floor($m / 60);
                $m = $m - $h * 60;
                $start_h = $start_h + $h;
                $temp_end = $end;
                if ($start != $end && $start_h < 24) {
                    $end = $start;
                }
                if ($start_h > 23) {
                    $flag = false;
                    $start_h = $start_h - 24;
                    $start = $temp_end;
                } else {
                    $flag = true;
                }
                if (strtotime($end) - strtotime($show_start) < 0) {
                    $end_show = $show_start . ' ' . str_pad($start_h, 2, "0", STR_PAD_LEFT) . ':' . str_pad($m, 2, "0", STR_PAD_LEFT) . ':00';
                } else {
                    $end_show = $end . ' ' . str_pad($start_h, 2, "0", STR_PAD_LEFT) . ':' . str_pad($m, 2, "0", STR_PAD_LEFT) . ':00';
                }
                $start_m = $m + rand(5, $range / 2);
                $end = $temp_end;

                $start_m = $start_m == 60 ? 63 : $start_m;
                if ($start_m > 60) {
                    $h = floor($start_m / 60);
                    $start_m = $start_m - $h * 60;
                    $start_h = $start_h + $h;
                    if ($start_h > 23) {
                        $flag = false;
                        $start_h = $start_h - 24;
                    } else {
                        $flag = true;
                    }
                }
                $t_1 = strtotime($end_show);
                $t_2 = strtotime($start_show);
                $t_h = floor(($t_1 - $t_2) / 3600);
                $user_score = $j == 7 ? $score[rand(0, 4)] : null;
                $new_file->setCellValue('A' . $line, $spreadsheet[$i][2]);
                $new_file->setCellValue('B' . $line, null);
                $new_file->setCellValue('C' . $line, $spreadsheet[$i][1]);
                $new_file->setCellValue('D' . $line, $course_number[$j]['name']);
                $new_file->setCellValue('E' . $line, $course_name[$course_number[$j]['name']]);
                $new_file->setCellValue('F' . $line, $start_show);
                $new_file->setCellValue('G' . $line, $end_show);
                $new_file->setCellValue('H' . $line, 'Yes');
                $new_file->setCellValue('I' . $line, str_pad($t_h, 2, "0", STR_PAD_LEFT) . ':' . str_pad((($t_1 - $t_2 - $t_h * 3600) / 60), 2, "0", STR_PAD_LEFT) . ':00');
                $new_file->setCellValue('J' . $line, $user_score);
                $line += 1;
            }
        }
        $file_write->save('data_convert.csv');
    }

    private function course() {
        return [
            ['name' => 'Scorm881', 'time' => 8],
            ['name' => 'Scorm862', 'time' => 19],
            ['name' => 'Scorm870', 'time' => 197],
            ['name' => 'Scorm863', 'time' => 85],
            ['name' => 'Scorm875', 'time' => 40],
            ['name' => 'Scorm876', 'time' => 70],
            ['name' => 'Scorm873', 'time' => 60],
            ['name' => 'Scorm882', 'time' => 30],
        ];
    }

    private function course_name() {
        return [
            'Scorm881' => 'Tổng quan về Chương trình đào tạo cơ bản và cơ chế đánh giá',
            'Scorm862' => 'Tổng quan về hoạt động kinh doanh theo phương thức đa cấp',
            'Scorm870' => 'Quy định pháp luật về hoạt động kinh doanh theo phương thức đa cấp',
            'Scorm863' => 'Các nội dung cơ bản của Hợp đồng tham gia bán hàng đa cấp',
            'Scorm875' => 'Các nội dung cơ bản trong Quy tắc hoạt động',
            'Scorm876' => 'Các nội dung cơ bản trong Kế hoạch trả thưởng',
            'Scorm873' => 'Các chuẩn mực đạo đức trong hoạt động bán hàng đa cấp',
            'Scorm882' => 'Bài kiểm tra kiến thức pháp luật bán hàng đa cấp',
        ];
    }

    public function format_advance() {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '512M');
        $reader = new Xlsx();
        //        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load(public_path('data_reset.xlsx'));
        $spreadsheet = $spreadsheet->getActiveSheet()->toArray();
        $new_file = new Spreadsheet();
        $file_write = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($new_file);
        $new_file->setActiveSheetIndex(0);
        $new_file = $new_file->getActiveSheet();
        $new_file->setCellValue('A1', 'First Name');
        $new_file->setCellValue('B1', 'Last Name');
        $new_file->setCellValue('C1', 'Employee ID');
        $new_file->setCellValue('D1', 'CourseNumber');
        $new_file->setCellValue('E1', 'CourseName');
        $new_file->setCellValue('F1', 'EnrollDate');
        $new_file->setCellValue('G1', 'CompletionDate');
        $new_file->setCellValue('H1', 'Passed');
        $new_file->setCellValue('I1', 'TrainingHrs');
        $new_file->setCellValue('J1', 'Score');

        $course_number = [
            'Scorm881' => 8,
            'Scorm862' => 19,
            'Scorm870' => 197,
            'Scorm863' => 85,
            'Scorm875' => 40,
            'Scorm876' => 70,
            'Scorm873' => 60,
            'Scorm882' => 30,
        ];
        $data_return = $this->format_data_spreadsheet($spreadsheet);
        $index = 2;
        foreach ($data_return as $sub_data) {
            if (!empty($sub_data)) {
                $start = explode(' ', $sub_data[0][5]);
                $start = $start[0];
                $count_line = count($sub_data);
                $end = explode(' ', $sub_data[$count_line - 1][6]);
                $end = $end[0];
                $start_h = null;
                $start_m = null;
                $flag = true;
                if ($start == $end) {
                    if (empty($start_h)) {
                        $r = rand(6, 12);
                        $start_h = rand(0, $r);
                    }
                    if (empty($start_m)) {
                        $start_m = str_pad(rand(0, 59), 2, "0", STR_PAD_LEFT);
                    }
                    $range = 12 - $start_h;
                    $range = floor((($range + 2) * 60 + 59) / 8);
                } else {
                    if (empty($start_h)) {
                        $start_h = rand(14, 23);
                    }
                    if (empty($start_m)) {
                        $start_m = rand(0, 59);
                    }
                    $range = floor(30 * 60 / 8);
                }

                for ($temp_i = 0; $temp_i < $count_line; $temp_i++) {
                    if (!empty($sub_data[$temp_i][0])) {
                        $show_start = $start;
                        if (!$flag) {
                            $show_start = $end;
                        }
                        $start_show = $show_start . ' ' . str_pad($start_h, 2, "0", STR_PAD_LEFT) . ':' . str_pad($start_m, 2, "0", STR_PAD_LEFT) . ':00';
                        $temp_r = rand(0, $range / 2);
                        if ($temp_i != 7) {
                            $m = $start_m + $course_number[$sub_data[$temp_i][3]] + $temp_r;
                        } else {
                            $m = $start_m + 30;
                        }
                        $h = floor($m / 60);
                        $m = $m - $h * 60;
                        $start_h = $start_h + $h;
                        $temp_end = $end;
                        if ($start != $end && $start_h < 24) {
                            $end = $start;
                        }
                        if ($start_h > 23) {
                            $flag = false;
                            $start_h = $start_h - 24;
                            $start = $temp_end;
                        } else {
                            $flag = true;
                        }
                        if (strtotime($end) - strtotime($show_start) < 0) {
                            $end_show = $show_start . ' ' . str_pad($start_h, 2, "0", STR_PAD_LEFT) . ':' . str_pad($m, 2, "0", STR_PAD_LEFT) . ':00';
                        } else {
                            $end_show = $end . ' ' . str_pad($start_h, 2, "0", STR_PAD_LEFT) . ':' . str_pad($m, 2, "0", STR_PAD_LEFT) . ':00';
                        }
                        $start_m = $m + rand(5, $range / 2);
                        $end = $temp_end;

                        $start_m = $start_m == 60 ? 63 : $start_m;
                        if ($start_m > 60) {
                            $h = floor($start_m / 60);
                            $start_m = $start_m - $h * 60;
                            $start_h = $start_h + $h;
                            if ($start_h > 23) {
                                $flag = false;
                                $start_h = $start_h - 24;
                            } else {
                                $flag = true;
                            }
                        }
                        $t_1 = strtotime($end_show);
                        $t_2 = strtotime($start_show);
                        $t_h = floor(($t_1 - $t_2) / 3600);
                        $new_file->setCellValue('A' . $index, $sub_data[$temp_i][0]);
                        $new_file->setCellValue('B' . $index, $sub_data[$temp_i][1]);
                        $new_file->setCellValue('C' . $index, $sub_data[$temp_i][2]);
                        $new_file->setCellValue('D' . $index, $sub_data[$temp_i][3]);
                        $new_file->setCellValue('E' . $index, $sub_data[$temp_i][4]);
                        $new_file->setCellValue('F' . $index, $start_show);
                        $new_file->setCellValue('G' . $index, $end_show);
                        $new_file->setCellValue('H' . $index, $sub_data[$temp_i][7]);
                        $new_file->setCellValue('I' . $index, str_pad($t_h, 2, "0", STR_PAD_LEFT) . ':' . str_pad((($t_1 - $t_2 - $t_h * 3600) / 60), 2, "0", STR_PAD_LEFT) . ':00');
                        $new_file->setCellValue('J' . $index, empty($sub_data[$temp_i][9]) ? null : $sub_data[$temp_i][9]);
                        $index++;
                    }
                }
            }
        }
        $file_write->save('format.xlsx');
    }

    public function format_data_spreadsheet($data) {
        unset($data[0]);
        $data_return = [];
        foreach ($data as $item) {
            $data_return[$item[2]][] = $item;
        }
        return $data_return;
    }

    public function format() {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '512M');
        $reader = new Xlsx();
        //        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load(public_path('data_reset.xlsx'));
        $spreadsheet = $spreadsheet->getActiveSheet()->toArray();
        $new_file = new Spreadsheet();
        $file_write = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($new_file);
        $new_file->setActiveSheetIndex(0);
        $new_file = $new_file->getActiveSheet();
        $new_file->setCellValue('A1', 'First Name');
        $new_file->setCellValue('B1', 'Last Name');
        $new_file->setCellValue('C1', 'Employee ID');
        $new_file->setCellValue('D1', 'CourseNumber');
        $new_file->setCellValue('E1', 'CourseName');
        $new_file->setCellValue('F1', 'EnrollDate');
        $new_file->setCellValue('G1', 'CompletionDate');
        $new_file->setCellValue('H1', 'Passed');
        $new_file->setCellValue('I1', 'TrainingHrs');
        $new_file->setCellValue('J1', 'Score');
        $count_data = count($spreadsheet);
        for ($i = 2; $i < $count_data + 2; $i++) {
            $date_1 = str_replace('AM', '', $spreadsheet[$i - 2][5]);
            $date_1 = str_replace('PM', '', $date_1);
            $date_2 = str_replace('AM', '', $spreadsheet[$i - 2][6]);
            $date_2 = str_replace('PM', '', $date_2);
            $new_file->setCellValue('A' . $i, $spreadsheet[$i - 2][0]);
            $new_file->setCellValue('B' . $i, $spreadsheet[$i - 2][1]);
            $new_file->setCellValue('C' . $i, $spreadsheet[$i - 2][2]);
            $new_file->setCellValue('D' . $i, $spreadsheet[$i - 2][3]);
            $new_file->setCellValue('E' . $i, $spreadsheet[$i - 2][4]);
            $new_file->setCellValue('F' . $i, trim($date_1));
            $new_file->setCellValue('G' . $i, trim($date_2));
            $new_file->setCellValue('H' . $i, $spreadsheet[$i - 2][7]);
            $new_file->setCellValue('I' . $i, $spreadsheet[$i - 2][8]);
            $new_file->setCellValue('J' . $i, empty($spreadsheet[$i - 2][9]) ? null : $spreadsheet[$i - 2][9]);
        }
        $file_write->save('format.xlsx');
    }

    public function reset_file() {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '512M');
        $reader = new Xlsx();
        //        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load(public_path('data_reset.xlsx'));
        $spreadsheet = $spreadsheet->getActiveSheet()->toArray();
        $new_file = new Spreadsheet();
        $file_write = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($new_file);
        $new_file->setActiveSheetIndex(0);
        $new_file = $new_file->getActiveSheet();
        $count_data = count($spreadsheet);
        //        dd($count_data);
        $count = $count_data / 8;
        $j = 1;
        for ($i = 1; $i <= $count; $i++) {
            $index = $i * 8 - 1;
            $temp = explode(' ', $spreadsheet[$index][5]);
            $temp_s = explode(' ', $spreadsheet[$index][6]);
            //name
            $new_file->setCellValue('A' . $j, $spreadsheet[$index][0]);
            //id
            $new_file->setCellValue('B' . $j, $spreadsheet[$index][2]);
            //score
            $new_file->setCellValue('C' . $j, $spreadsheet[$index][9]);
            //start
            $new_file->setCellValue('D' . $j, $temp[0]);
            //end
            $new_file->setCellValue('E' . $j, $temp_s[0]);
            $j++;
        }
        //        $score = [80, 85, 90, 95, 100];
        //        for ($i = 0; $i < $count; $i++) {
        //            $index = $i * 8;
        //            $temp = explode(' ', $spreadsheet[$index][5]);
        //            $temp_s = explode(' ', $spreadsheet[$index][6]);
        //            $new_file->setCellValue('A' . $j, $spreadsheet[$index][0]);
        //            $new_file->setCellValue('B' . $j, $spreadsheet[$index][2]);
        //            $new_file->setCellValue('C' . $j, $score[rand(0, 4)]);
        //            $new_file->setCellValue('D' . $j, $temp[0]);
        //            $new_file->setCellValue('E' . $j, $temp_s[0]);
        //            $j++;
        //        }
        $file_write->save('reset.xlsx');
    }

    public function reset_time() {
        $file = 'reset';
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '512M');
        $reader = new Xlsx();
        //        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load(public_path($file . '.xlsx'));
        $spreadsheet = $spreadsheet->getActiveSheet()->toArray();
        $course_number = $this->course();
        $course_name = $this->course_name();
        //First_name => 2
        //Last_name => null
        //ID => 1
        //course_number => $couerse_number
        //course_name => $course_name
        //EnrollDate => 6
        //CompletionDate => 7
        //Passed => "Yes"
        //TrainingHrs => 7-6
        //Score => null
        $count = count($spreadsheet);
        $new_file = new Spreadsheet();
        $file_write = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($new_file);
        $new_file->setActiveSheetIndex(0);
        $new_file = $new_file->getActiveSheet();
        $new_file->setCellValue('A1', 'First Name');
        $new_file->setCellValue('B1', 'Last Name');
        $new_file->setCellValue('C1', 'Employee ID');
        $new_file->setCellValue('D1', 'CourseNumber');
        $new_file->setCellValue('E1', 'CourseName');
        $new_file->setCellValue('F1', 'EnrollDate');
        $new_file->setCellValue('G1', 'CompletionDate');
        $new_file->setCellValue('H1', 'Passed');
        $new_file->setCellValue('I1', 'TrainingHrs');
        $new_file->setCellValue('J1', 'Score');
        $line = 2;
        for ($i = 0; $i < $count; $i++) {
            if (!empty($spreadsheet[$i][0])) {
                $start = $spreadsheet[$i][3];
                $end = $spreadsheet[$i][4];
                $start_h = null;
                $start_m = null;
                $flag = true;
                if ($start == $end) {
                    if (empty($start_h)) {
                        $r = rand(0, 12);
                        $start_h = rand(0, $r);
                    }
                    if (empty($start_m)) {
                        $start_m = str_pad(rand(0, 59), 2, "0", STR_PAD_LEFT);
                    }
                    $range = 12 - $start_h;
                    $range = floor((($range + 2) * 60 + 59) / 8);
                } else {
                    if (empty($start_h)) {
                        $start_h = rand(19, 23);
                    }
                    if (empty($start_m)) {
                        $start_m = rand(0, 59);
                    }
                    $range = floor(15 * 60 / 8);
                }
                for ($j = 0; $j < 8; $j++) {
                    $show_start = $start;
                    if (!$flag) {
                        $show_start = $end;
                    }
                    $start_show = $show_start . ' ' . str_pad($start_h, 2, "0", STR_PAD_LEFT) . ':' . str_pad($start_m, 2, "0", STR_PAD_LEFT) . ':00';
                    $temp_r = rand(0, $range / 2);
                    if ($j != 7) {
                        $m = $start_m + $course_number[$j]['time'] + $temp_r;
                    } else {
                        $m = $start_m + 30;
                    }
                    $h = floor($m / 60);
                    $m = $m - $h * 60;
                    $start_h = $start_h + $h;
                    $temp_end = $end;
                    if ($start != $end && $start_h < 24) {
                        $end = $start;
                    }
                    if ($start_h > 23) {
                        $flag = false;
                        $start_h = $start_h - 24;
                        $start = $temp_end;
                    } else {
                        $flag = true;
                    }
                    if (strtotime($end) - strtotime($show_start) < 0) {
                        $end_show = $show_start . ' ' . str_pad($start_h, 2, "0", STR_PAD_LEFT) . ':' . str_pad($m, 2, "0", STR_PAD_LEFT) . ':00';
                    } else {
                        $end_show = $end . ' ' . str_pad($start_h, 2, "0", STR_PAD_LEFT) . ':' . str_pad($m, 2, "0", STR_PAD_LEFT) . ':00';
                    }
                    $start_m = $m + rand(5, $range / 2);
                    $end = $temp_end;

                    $start_m = $start_m == 60 ? 63 : $start_m;
                    if ($start_m > 60) {
                        $h = floor($start_m / 60);
                        $start_m = $start_m - $h * 60;
                        $start_h = $start_h + $h;
                        if ($start_h > 23) {
                            $flag = false;
                            $start_h = $start_h - 24;
                        } else {
                            $flag = true;
                        }
                    }
                    $t_1 = strtotime($end_show);
                    $t_2 = strtotime($start_show);
                    $t_h = floor(($t_1 - $t_2) / 3600);
                    $user_score = $j == 7 ? $spreadsheet[$i][2] : null;
                    $new_file->setCellValue('A' . $line, $spreadsheet[$i][0]);
                    $new_file->setCellValue('B' . $line, null);
                    $new_file->setCellValue('C' . $line, $spreadsheet[$i][1]);
                    $new_file->setCellValue('D' . $line, $course_number[$j]['name']);
                    $new_file->setCellValue('E' . $line, $course_name[$course_number[$j]['name']]);
                    $new_file->setCellValue('F' . $line, $start_show);
                    $new_file->setCellValue('G' . $line, $end_show);
                    $new_file->setCellValue('H' . $line, 'Yes');
                    $new_file->setCellValue('I' . $line, str_pad($t_h, 2, "0", STR_PAD_LEFT) . ':' . str_pad((($t_1 - $t_2 - $t_h * 3600) / 60), 2, "0", STR_PAD_LEFT) . ':00');
                    $new_file->setCellValue('J' . $line, $user_score);
                    $line += 1;
                }
            }
        }
        $file_write->save('convert_time.xlsx');
    }

    public function excell() {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '512M');
        $reader = new Xlsx();
        //        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load(public_path('gk_data.xlsx'));
        $spreadsheet = $spreadsheet->getActiveSheet()->toArray();
        $total_info = 7;
        $count = count($spreadsheet);
        $data_insert = [];
        $j = 0;
        for ($i = 0; $i < $count; $i++) {
            if ($i < $total_info) {
                if ($i == $total_info - 7) {
                    $data_insert[$j][] = $spreadsheet[$i][0];
                } else {
                    $data = explode(':', $spreadsheet[$i][0]);
                    $data_insert[$j][] = !empty($data[1]) ? trim($data[1]) : '';
                }
            }
            if ($i == $total_info - 1) {
                $total_info += 7;
                $j += 1;
            }
        }
        $new_file = new Spreadsheet();
        $file_write = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($new_file);
        $new_file->setActiveSheetIndex(0);
        $new_file = $new_file->getActiveSheet();
        $new_file->setCellValue('A1', 'TÊN CÔNG TY');
        $new_file->setCellValue('B1', 'MÃ SỐ THUẾ');
        $new_file->setCellValue('C1', 'TỈNH THÀNH');
        $new_file->setCellValue('D1', 'NGÀY LẬP');
        $new_file->setCellValue('E1', 'ĐỊA CHỈ');
        $new_file->setCellValue('F1', 'ĐẠI DIỆN');
        $new_file->setCellValue('G1', 'SĐT');
        $count_data = count($data_insert);
        for ($i = 2; $i < $count_data + 2; $i++) {
            $new_file->setCellValue('A' . $i, $data_insert[$i - 2][0]);
            $new_file->setCellValue('B' . $i, $data_insert[$i - 2][1]);
            $new_file->setCellValue('C' . $i, $data_insert[$i - 2][2]);
            $new_file->setCellValue('D' . $i, $data_insert[$i - 2][3]);
            $new_file->setCellValue('E' . $i, $data_insert[$i - 2][4]);
            $new_file->setCellValue('F' . $i, $data_insert[$i - 2][5]);
            $new_file->setCellValue('G' . $i, $data_insert[$i - 2][6]);
        }
        $file_write->save('quan_thanh_xuan.xlsx');
        //        dd($data_insert);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //        $model = new Users();
        $users = DB::table('users')->where('id >', 10)->get();
        dd($users);
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $users = $this->repository->paginate(20);
        //        if (request()->wantsJson()) {
        //
        //            return response()->json([
        //                'data' => $users,
        //            ]);
        //        }
        $this->title = 'User manager';
        $content = view('pages.demo', ['users' => $users]);
        return $this->show_page($content);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UsersCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(UsersCreateRequest $request) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $user = $this->repository->create($request->all());

            $response = [
                'message' => 'Users created.',
                'data'    => $user->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $user = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $user,
            ]);
        }

        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $user = $this->repository->find($id);

        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UsersUpdateRequest $request
     * @param  string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(UsersUpdateRequest $request, $id) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $user = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Users updated.',
                'data'    => $user->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Users deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Users deleted.');
    }
}

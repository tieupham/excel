<?php

namespace App\Http\Controllers\Base;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

abstract class LayoutsController extends Controller {
    protected $list_css = [];
    protected $list_js = [];
    protected $view_name = 'layouts.master';
    protected $title;
    protected $data = [
        'load_more_css' => '',
        'load_more_js'  => '',
    ];


    public function __construct() {
        parent::__construct();
        $this->_set_side_bar_left();
    }

    private function _set_side_bar_left() {
        $menu[] = Array(
            "text" => "Dashboard",
            "icon" => "fa-tachometer",
            "url"  => route('home'),
        );
        $menu[] = Array(
            "text"  => "Quản lý đơn hàng",
            "icon"  => "fa-file-text-o",
            "url"   => route('home'),
            "role"  => ['corporation', 'ppc'],
            "child" => Array(
                '0' => Array(
                    "text"  => "Quản lý đơn hàng",
                    "icon"  => "fa-caret-right",
                    "url"   => route('home'),
                    "class"  => "active",
                    "child" => Array(
                        '0' => Array(
                            "text" => "Tổng quan đơn hàng",
                            "icon" => "fa-caret-right",
                            "url"  => route('home'),
                        ),
                        '1' => Array(
                            "text" => "Thông tin đơn hàng",
                            "icon" => "fa-caret-right",
                            "url"  => route('home'),
                        ),
                    ),
                ),
                '1' => Array(
                    "text" => "Tổng quan đơn hàng",
                    "icon" => "fa-caret-right",
                    "url"  => route('home'),
                ),
                '2' => Array(
                    "text" => "Thông tin đơn hàng",
                    "icon" => "fa-caret-right",
                    "url"  => route('home'),
                ),
            ),
        );
        $this->data['menu_data'] = $menu;
    }

    protected function load_file_css() {
        if (!empty($this->list_css)) {
            foreach ($this->list_css as $item) {
                $this->data['load_more_css'] .= '<link rel="stylesheet" href="' . public_path($item) . '"/>';
            }
        }
    }

    protected function load_file_js() {
        if (!empty($this->list_js)) {
            foreach ($this->list_js as $item) {
                $this->data['load_more_js'] .= '<script src="' . public_path($item) . '"></script>';
            }
        }
    }

    private function set_page_title() {
        $this->data['title'] = $this->title;
    }

    protected function show_page($content) {
        $this->set_page_title();
        $this->load_file_css();
        $this->load_file_js();
        $this->data['content'] = $content;
        return view($this->view_name, $this->data);
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base\LayoutsController;
use Illuminate\Http\Request;

class DemoController extends LayoutsController {
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->title = 'Olala';
        $content = view('pages.demo', ['test' => 'dhasgdhjasjhds']);
        return $this->show_page($content);
    }
}

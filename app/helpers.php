<?php

if (!function_exists('getAvatarUrl')) {
    function getAvatarUrl($name)
    {
        if (!empty($name)) return Avatar::create($name)->toBase64();
    }
}

if (!function_exists('convertStringToNumber')) {
    function convertStringToNumber($str)
    {
        return preg_replace("/([^0-9\\.])/i", "", $str);
    }
}

if (!function_exists('generateRandomString')) {
    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', ['as'=>'home','uses' => 'DemoController@index']);
//Route::get('/', ['as'=>'home','uses' => 'UsersController@index']);
//Route::get('/excell', ['as'=>'home','uses' => 'UsersController@format']);
//Route::get('/reset', ['as'=>'home','uses' => 'UsersController@reset_file']);
//Route::get('/format_advance', ['as'=>'home','uses' => 'UsersController@format_advance']);
//Route::get('/reset_time', ['as'=>'home','uses' => 'UsersController@reset_time']);
//Route::get('/import-format', ['as'=>'home','uses' => 'UsersController@import_format']);
//Route::post('/format-file', ['as'=>'upload.format','uses' => 'UsersController@upload_file_format']);

Route::get('/format-cell', ['as'=>'home','uses' => 'UsersController@format_cell']);
Route::get('/format-time', ['as'=>'home','uses' => 'UsersController@format_time']);
Route::get('/make-file', ['as'=>'home','uses' => 'UsersController@make_file']);
Route::get('/make-file-new-course', ['as'=>'home','uses' => 'UsersController@make_file_add_two_course']);
Route::get('/make-file-new-course-advance', ['as'=>'home','uses' => 'UsersController@make_file_add_two_course_advance']);
Route::get('/change-course', ['as'=>'home','uses' => 'UsersController@change_course']);

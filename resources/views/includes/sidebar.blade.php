<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">メニュー</li>
            <!-- render menu -->
            <?php function renderCategory($categoryList, $is_child = FALSE) { ?>
            <?php foreach ($categoryList as $item) { ?>
            <li class="<?php echo empty($item["child"]) ? '' : 'treeview'; ?> <?php echo empty($item["class"]) ? '' : $item["class"]; ?>">
                <a class="<?php
                echo isset($item["class"]) ? $item["class"] : '';?>"
                   href="<?php echo empty($item["child"]) ? $item["url"] : "#"; ?>"
                   title="<?php echo $item["text"] ?>">
                    <?php if (!$is_child) { ?>
                    <i class="fa <?php echo $item["icon"] ?>"></i>
                    <span><?php echo $item["text"] ?></span>
                    <?php } else { ?>
                    <i class="menu-icon fa fa-caret-right"></i>
                    <?php echo $item["text"] ?>
                    <?php } ?>
                    <?php if(!empty($item['child'])){ ?>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    <?php } ?>
                </a>
                <?php if (!empty($item["child"])) {
                    echo '<ul class="treeview-menu">';
                    renderCategory($item["child"], TRUE);
                    echo '</ul>';
                } ?>
            </li>
        <?php }
        } ?>
        <?php renderCategory($menu_data); ?>
        <!-- end render menu -->
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
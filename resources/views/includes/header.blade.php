<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{--{{route('dashboard')}}--}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
            <b>SKS</b>
        </span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
            <b>仕分管理SKS</b>
        </span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <button class="glyphicon glyphicon-resize-full" id="fullscreen"
                style="padding: 15px 15px; color: #fff;background-color: transparent;border: none;"></button>
        <button class="glyphicon glyphicon-resize-small" id="hidefullscreen"
                style="padding: 15px 15px; color: #fff;background-color: transparent;display: none;border: none;"></button>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav" style="font-size: 12px;">
                <li class="messages-menu">
                    <a href="javascript:void(0)">会社名/: </a>
                </li>
                <li class="messages-menu">
                    <a href="javascript:void(0)">氏名：</a>
                </li>
                <li class="messages-menu">
                    <a href="javascript:void(0)">所属名：</a>
                </li>
                <li class="messages-menu">
                    <a href="javascript:void(0)">日付：</a>
                </li>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{getAvatarUrl('Admin')}}" class="user-image" alt="User Image">
                        <span class="hidden-xs">Admin</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        {{--<li class="user-header">--}}
                            {{--<img src="{{getAvatarUrl('Admin')}}" class="img-circle" alt="User Image">--}}
                            {{--<p>--}}
                                {{--Alexander Pierce - Web Developer--}}
                                {{--<small>Member since Nov. 2012</small>--}}
                            {{--</p>--}}
                        {{--</li>--}}
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">システム選択</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{--{{ route('logout') }}--}}" class="btn btn-default btn-flat">ログアウト</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

    </nav>
</header>
<footer class="main-footer">
    <!-- To the right -->
    <!-- <div class="pull-right hidden-xs">
        Anything you want
    </div> -->
    <!-- Default to the left -->
    <strong>Copyright © {{date('Y')}} <a href="">{{config('app.name')}}</a>.</strong> All rights reserved.
</footer>
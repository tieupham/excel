<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="msg-confirm" content="{{ __('messages.msg_confirm') }}">
    <meta name="msg-notify-header" content="{{ __('messages.msg_notify_header') }}">
    <title>{{$title}}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css") }}" rel="stylesheet"
          type="text/css"/>
    <!-- Theme style -->
    <link href="{{ asset("/adminlte/bower_components/select2/dist/css/select2.min.css")}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset("/css/jquery.jgrowl.css")}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{asset('/adminlte/plugins/iCheck/all.css')}}">
    <link href="{{ asset("/adminlte/dist/css/AdminLTE.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset("/css/base.css")}}" rel="stylesheet" type="text/css"/>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css") }}" rel="stylesheet"
          type="text/css"/>
    <!-- Font Awesome Icons -->
    <link href="{{ asset("/adminlte/bower_components/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet"
          type="text/css"/>
    <!-- Ionicons -->
    <link href="{{ asset("/adminlte/bower_components/Ionicons/css/ionicons.min.css") }}" rel="stylesheet"
          type="text/css"/>
    <!-- Theme style -->
    <link href="{{ asset("/adminlte/bower_components/select2/dist/css/select2.min.css")}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset("/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css")}}"
          rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{asset('/adminlte/plugins/iCheck/all.css')}}">
    <link href="{{ asset("/adminlte/dist/css/AdminLTE.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset("/adminlte/dist/css/skins/_all-skins.min.css")}}" rel="stylesheet" type="text/css"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <?php echo $load_more_css ?>
    @yield('css')
</head>
<body class="skin-blue sidebar-mini">
<!-- Main style css follow campaign color picked -->
<div class="wrapper">
    <!-- Header -->
@include('includes.header')

<!-- Sidebar -->
@include('includes.sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h4>
                @yield('path')
                <small></small>
            </h4>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            <?php echo $content; ?>
        </section><!-- /.content -->
        <div id="confirmDialog" class="modal">
            <div class="modal-dialog modal-md">
                <div id="confirmBox" class="modal-content">
                    <div class="modal-body modal-md-pd text-center">
                        <button type="button" class="close confirmClose" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <br>
                        <span class="message"></span>
                    </div>
                    <div class="modal-footer modal-md-tf">
                        <button type="button" class="btn btn-success pull-left yes">YES</button>
                        <button type="button" class="btn btn-danger pull-right no">NO</button>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.content-wrapper -->

    <!-- Footer -->
    @include('includes.footer')

    <div class="wait modal-backdrop fade in text-center e_loading" style="padding-top:25%;display: none; opacity: 0.6">
        <i class="fa fa-3x fa-refresh fa-spin" style="z-index:1560!important;color:white!important"></i>
    </div>

</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.3 -->
<script src="{{ asset ("/adminlte/bower_components/jquery/dist/jquery.min.js") }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset ("/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js") }}"
        type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/adminlte/dist/js/adminlte.min.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/js/library/jquery.fullscreen.min.js")}}" type="text/javascript"></script>
<script src="{{ asset ("/js/library/bootstrap-filestyle.min.js")}}" type="text/javascript"></script>
<script src="{{ asset ("/adminlte/bower_components/select2/dist/js/select2.full.min.js") }}"></script>
<script src="{{ asset ('/adminlte/bower_components/select2/dist/js/i18n/ja.js') }}" type="text/javascript"></script>
<script src="{{asset("/adminlte/plugins/iCheck/icheck.min.js")}}"></script>
<script src="{{asset("/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js")}}"></script>
<script src="{{asset("/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js")}}"></script>
<script src="{{asset("/js/library/jquery.jgrowl.min.js")}}"></script>
<script src="{{asset('/js/base64/base64.min.js')}}" type="text/javascript"></script>
<script src="{{ asset ("/js/base_js.js") }}"></script>
{{--Load common confirm--}}
{{--<script src="{{ asset ('/js/common.confirm.js') }}"></script>--}}
<?php echo $load_more_js ?>
@yield('javascript')
<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
</script>
</body>
</html>